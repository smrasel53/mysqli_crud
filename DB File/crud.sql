-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2017 at 03:59 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `go`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `studentId` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `sex` varchar(15) NOT NULL,
  `dept` varchar(100) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`studentId`, `fullname`, `sex`, `dept`, `skill`, `is_delete`) VALUES
(1, 'Abu Sofian', 'Male', 'CSE', 'PHP,Java', 0),
(2, 'Abdullah Al Mamun Rashid', 'Others', 'BBA', 'PHP,Java,Perl,Python', 0),
(3, 'Asma Ahmed Eva', 'Female', 'BBA', 'PHP,Java,Perl', 0),
(4, 'Tamanna Akter', 'Female', 'EEE', 'Java,Python', 0),
(8, 'MH Dhoni', 'Male', 'BBA', 'Perl', 1),
(14, 'Delowar Hossian', 'Male', 'BBA', 'PHP,Java', 0),
(15, 'Halima Akter', 'Female', 'CSE', 'PHP,Java', 0),
(16, 'SM Rasel', 'Male', 'CSE', 'PHP,Python', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`studentId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `studentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
